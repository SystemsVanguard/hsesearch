# hsesearch

A web login and authentication app.  Back-end in Node.js with Express framework.  Front-end in React.js.  Authentication by the Passport.js library. Also uses Bootstrap 4, Google Fonts, and a Fontastic.me generated font.  Property of McMaster - HSE (